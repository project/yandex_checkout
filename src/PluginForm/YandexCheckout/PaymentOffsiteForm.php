<?php

namespace Drupal\yandex_checkout\PluginForm\YandexCheckout;

use YooKassa\Model\Payment;
use YooKassa\Model\ConfirmationType;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Request\Payments\CreatePaymentRequest;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\yandex_checkout\Plugin\Commerce\PaymentGateway\YandexCheckout;
use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;

/**
 * Offsite Payment.
 */
class PaymentOffsiteForm extends BasePaymentOffsiteForm {

  /**
   * Form Build.
   *
   * @param array $form
   *   Plugin config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form State.
   *
   * @return array
   *   Form build.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   * @throws \Exception
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    try {
      $form = parent::buildConfigurationForm($form, $form_state);

      /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
      $payment = $this->entity;
      /** @var \Drupal\yandex_checkout\Plugin\Commerce\PaymentGateway\YandexCheckout $paymentGatewayPlugin */
      $paymentGatewayPlugin = $payment->getPaymentGateway()->getPlugin();
      $client               = $paymentGatewayPlugin->apiClient;
      $order                = $payment->getOrder();
      $amount               = $payment->getAmount();
      $config               = $paymentGatewayPlugin->getConfiguration();

      /** @var \YooKassa\Request\Payments\CreatePaymentRequestBuilder $builder */
      $builder = CreatePaymentRequest::builder()
        ->setCapture(TRUE)
        ->setDescription($this->createDescription($order, $config))
        ->setConfirmation([
          'type'      => ConfirmationType::REDIRECT,
          'returnUrl' => $form['#return_url'],
        ])
        ->setMetadata([
          'cms_name'       => 'ya_api_drupal8',
          'module_version' => YandexCheckout::YAMONEY_MODULE_VERSION,
        ]);
      $builder->setAmount($amount->getNumber());
      if ($config['receipt_enabled'] == 1) {
        /** @var \Drupal\user\UserInterface $profile */
        $profile = $order->getCustomer();
        $builder->setReceiptEmail($order->getEmail());
        $items = $order->getItems();
        /** @var \Drupal\commerce_order\Entity\OrderItem $item */
        foreach ($items as $item) {
          /** @var \Drupal\commerce_order\AdjustmentItemList $adjustments */
          $adjustments = $item->get('adjustments');

          $taxUuid    = NULL;
          $percentage = 0;
          foreach ($adjustments->getValue() as $adjustmentValue) {
            /** @var \Drupal\commerce_order\Adjustment $adjustment */
            $adjustment = $adjustmentValue['value'];
            if ($adjustment->getType() == 'tax') {
              $sourceId   = explode('|', $adjustment->getSourceId());
              $taxUuid    = $sourceId[2];
              $percentage = $adjustment->getPercentage();
            }
          }
          $vat_code = $config['default_tax'];
          if ($taxUuid && in_array($taxUuid, array_keys($config['yandex_checkout_tax']))) {
            // $vat_code = $config['yandex_checkout_tax'][$taxUuid];
            // @todo CommerceVAT to YaID:
            // 1  Без НДС
            // 2  НДС по ставке 0%
            // 3  НДС по ставке 10%
            // 4  НДС чека по ставке 20%
            // 5  НДС чека по расчетной ставке 10/110
            // 6  НДС чека по расчетной ставке 20/120
          }
          $priceWithTax = $item->getUnitPrice()->getNumber() * (1 + $percentage);
          $builder->addReceiptItem($item->getTitle(), $priceWithTax, $item->getQuantity(), $vat_code);
        }
      }
      $paymentRequest = $builder->build();
      if (($config['receipt_enabled'] == 1) && $paymentRequest->getReceipt() !== NULL) {
        $paymentRequest->getReceipt()->normalize($paymentRequest->getAmount());
      }
      $response = $client->createPayment($paymentRequest);

      $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
      $payments = $payment_storage->loadByProperties(['order_id' => $order->id()]);
      $total = 0;
      if ($payments) {
        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payment = reset($payments);
        $payment->enforceIsNew(FALSE);
        $payment->setRemoteId($response->getId());
        $payment->setRemoteState($response->getStatus());
        $payment->save();
        $total = $payment->getAmount()->getNumber();
      }
      $redirect_url = $response->confirmation->confirmationUrl;
      $data         = [
        'return' => $form['#return_url'],
        'cancel' => $form['#cancel_url'],
        'total'  => $total,
      ];

      return $this->buildRedirectForm($form, $form_state, $redirect_url, $data);
    }
    catch (ApiException $e) {
      \Drupal::logger('yandex_checkout')->error('Api error: ' . $e->getMessage());
      $message = $this->t('Не удалось создать платеж.');
      \Drupal::messenger()->addError($message);
      throw new PaymentGatewayException();
    }
  }

  /**
   * Description.
   *
   * @param Drupal\commerce_order\Entity\OrderInterface $order
   *   Commerce Order Entity.
   * @param array $config
   *   Plugin config.
   *
   * @return string
   *   Description.
   */
  private function createDescription(OrderInterface $order, array $config) {
    $descriptionTemplate = !empty($config['description_template'])
            ? $config['description_template']
            : t('Оплата заказа №%order_id%');

    $replace = [];
    foreach ($order as $property => $fieldItems) {
      foreach ($fieldItems as $key => $fieldItem) {
        if (!($fieldItem instanceof FieldItemInterface)) {
          continue;
        }
        $params = $fieldItem->getEntity()->toArray();
        if (empty($params[$property])) {
          continue;
        }
        if (!is_array($params[$property])) {
          continue;
        }
        if (empty($params[$property][0])) {
          continue;
        }
        $fieldData = $params[$property][0];
        if (!is_array($fieldData)) {
          continue;
        }
        $value = current($fieldData);
        if (!is_scalar($value)) {
          continue;
        }
        $replace['%' . $property . '%'] = $value;
      }
    }

    $description = strtr($descriptionTemplate, $replace);

    return (string) mb_substr($description, 0, Payment::MAX_LENGTH_DESCRIPTION);
  }

}
