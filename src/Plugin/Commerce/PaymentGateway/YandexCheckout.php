<?php

namespace Drupal\yandex_checkout\Plugin\Commerce\PaymentGateway;

use Drupal\Core\Url;
use YooKassa\Client;
use YooKassa\Model\PaymentStatus;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\commerce_payment\PaymentTypeManager;
use YooKassa\Model\NotificationEventType;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use YooKassa\Model\Notification\NotificationSucceeded;
use YooKassa\Request\Payments\Payment\CreateCaptureRequest;
use YooKassa\Model\Notification\NotificationWaitingForCapture;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;

/**
 * Ya Payment Gateway.
 *
 * @CommercePaymentGateway(
 *   id = "yandex_checkout",
 *   label = "Yandex Checkout",
 *   display_label = "Yandex Checkout",
 *   forms = {
 *     "offsite-payment" = "Drupal\yandex_checkout\PluginForm\YandexCheckout\PaymentOffsiteForm",
 *     "test-action" = "Drupal\yandex_checkout\PluginForm\YandexCheckout\PaymentMethodAddForm"
 *   },
 *   payment_method_types = {
 *     "yandex_checkout_epl"
 *   },
 *   requires_billing_information = FALSE
 * )
 */
class YandexCheckout extends OffsitePaymentGatewayBase implements ContainerFactoryPluginInterface {
  const YAMONEY_MODULE_VERSION = '1.1.1';

  /**
   * Yandex API.
   *
   * @var YooKassa\Client
   */
  public $apiClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        EntityTypeManagerInterface $entity_type_manager,
        PaymentTypeManager $payment_type_manager,
        PaymentMethodTypeManager $payment_method_type_manager,
        TimeInterface $time
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager,
          $payment_method_type_manager, $time);
    $shopId               = $this->configuration['shop_id'];
    $secretKey            = $this->configuration['secret_key'];
    $yandexCheckoutClient = new Client();
    $yandexCheckoutClient->setAuth($shopId, $secretKey);
    $userAgent = $yandexCheckoutClient->getApiClient()->getUserAgent();
    $userAgent->setCms('Drupal', \Drupal::VERSION);
    $userAgent->setModule('yandex-money-cms-v2', self::YAMONEY_MODULE_VERSION);
    $this->apiClient = $yandexCheckoutClient;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shop_id'             => '',
      'secret_key'          => '',
      'description_template' => '',
      'receipt_enabled'     => '',
      'default_tax'         => '',
      'yandex_checkout_tax' => [],
      'notification_url'    => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['shop_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('shopId'),
      '#default_value' => $this->configuration['shop_id'],
      '#required'      => TRUE,
    ];

    $form['secret_key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Secret Key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required'      => TRUE,
    ];

    $form = parent::buildConfigurationForm($form, $form_state);
    $form['description_template'] = [
      '#type'          => 'textfield',
      '#title'         => t('Описание платежа'),
      '#description'   => t('Это описание транзакции, которое пользователь увидит при оплате, а вы — в личном кабинете Яндекс.Кассы. Например, «Оплата заказа №72».<br>
    Чтобы в описание подставлялся номер заказа (как в примере), поставьте на его месте %order_id% (Оплата заказа №%order_id%).<br>
    Ограничение для описания — 128 символов.'),
      '#default_value' => !empty($this->configuration['description_template'])
      ? $this->configuration['description_template']
      : $this->t('Оплата заказа №%order_id%'),
    ];

    $form['receipt_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Отправлять в Яндекс.Кассу данные для чеков (54-ФЗ)'),
      '#default_value' => $this->configuration['receipt_enabled'],
    ];
    if ($this->configuration['receipt_enabled']) {
      $form['default_tax'] = [
        '#type'          => 'select',
        '#title'         => 'Ставка по умолчанию',
        '#options'       => [
          1 => t('Без НДС'),
          2 => t('0%'),
          3 => t('10%'),
          4 => t('20%'),
          5 => t('Расчётная ставка 10/110'),
          6 => t('Расчётная ставка 20/120'),
        ],
        '#default_value' => $this->configuration['default_tax'],
      ];
      $this->taxIsEnabled($form);
    }
    $entityId = $this->parentEntity->setId($form_state->getValue('id'));
    if ($entityId) {
      $form['notification_url'] = [
        '#type'          => 'textfield',
        '#title'         => t('Url для нотификаций'),
        // '#default_value' => $this->getNotifyUrl()->toString(),
        '#attributes'    => ['readonly' => 'readonly'],
      ];
    }
    $logurl = "/admin/reports/dblog?type[]=yandex_checkout";
    $form['log_file'] = [
      '#type' => 'item',
      '#title' => t('Логирование'),
      '#markup' => t('Посмотреть <a href=":link" target="_blank">записи журнала</a>.', [
        ':link"' => $logurl,
      ]),
    ];

    return $form;
  }

  /**
   * Form Validate.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue($form['#parents']);
    if (!preg_match('/^test_.*|live_.*$/i', $values['secret_key'])) {
      $markup = new TranslatableMarkup('Такого секретного ключа нет. Если вы уверены, что скопировали ключ правильно, значит, он по какой-то причине не работает.
                  Выпустите и активируйте ключ заново —
                  <a href="https://money.yandex.ru/joinups">в личном кабинете Яндекс.Кассы</a>');
      $form_state->setError($form['secret_key'], $markup);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values                                      = $form_state->getValue($form['#parents']);
      $this->configuration['shop_id']              = $values['shop_id'];
      $this->configuration['secret_key']           = $values['secret_key'];
      $this->configuration['description_template'] = $values['description_template'];
      $this->configuration['receipt_enabled']      = $values['receipt_enabled'];
      $this->configuration['default_tax']          = isset($values['default_tax']) ? $values['default_tax'] : '';
      $this->configuration['yandex_checkout_tax']  = isset($values['yandex_checkout_tax']) ? $values['yandex_checkout_tax'] : '';
    }
  }

  /**
   * Processes the "return" request.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @throws \Drupal\commerce\Response\NeedsRedirectException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \YooKassa\Common\Exceptions\ApiException
   * @throws \YooKassa\Common\Exceptions\BadApiRequestException
   * @throws \YooKassa\Common\Exceptions\ForbiddenException
   * @throws \YooKassa\Common\Exceptions\InternalServerError
   * @throws \YooKassa\Common\Exceptions\NotFoundException
   * @throws \YooKassa\Common\Exceptions\ResponseProcessingException
   * @throws \YooKassa\Common\Exceptions\TooManyRequestsException
   * @throws \YooKassa\Common\Exceptions\UnauthorizedException
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_storage = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments        = $payment_storage->loadByProperties(['order_id' => $order->id()]);
    if ($payments) {
      $payment = reset($payments);
    }
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $paymentId           = $payment->getRemoteId();
    $apiClient           = $this->apiClient;
    $cancelUrl           = $this->buildCancelUrl($order);
    $paymentInfoResponse = $apiClient->getPaymentInfo($paymentId);
    $this->log('Payment info: ' . json_encode($paymentInfoResponse));
    if ($paymentInfoResponse->status == PaymentStatus::WAITING_FOR_CAPTURE) {
      $captureRequest = CreateCaptureRequest::builder()->setAmount($paymentInfoResponse->getAmount())->build();
      $paymentInfoResponse = $apiClient->capturePayment($captureRequest, $paymentId);
      $this->log('Payment info after capture: ' . json_encode($paymentInfoResponse));
    }
    if ($paymentInfoResponse->status == PaymentStatus::SUCCEEDED) {
      $payment->setRemoteState($paymentInfoResponse->status);
      $payment->setState('completed');
      $payment->save();
      $this->log('Payment completed');
    }
    elseif ($paymentInfoResponse->status == PaymentStatus::PENDING && $paymentInfoResponse->getPaid()) {
      $payment->setRemoteState($paymentInfoResponse->status);
      $payment->setState('pending');
      $payment->save();
      $this->log('Payment pending');
    }
    elseif ($paymentInfoResponse->status == PaymentStatus::CANCELED) {
      $payment->setRemoteState($paymentInfoResponse->status);
      $payment->setState('canceled');
      $payment->save();
      $this->log('Payment canceled');
      throw new NeedsRedirectException($cancelUrl->toString());
    }
    else {
      $this->log('Wrong payment status: ' . $paymentInfoResponse->status);
      throw new NeedsRedirectException($cancelUrl->toString());
    }
  }

  /**
   * Taxes if Enabled.
   */
  public function taxIsEnabled(&$from) {
    if (\Drupal::moduleHandler()->moduleExists('commerce_tax')) {
      $tax_storage = $this->entityTypeManager->getStorage('commerce_tax_type');
      $taxTypes = $tax_storage->loadMultiple();
      $taxRates = [];
      foreach ($taxTypes as $taxType) {
        /** @var \Drupal\commerce_tax\Entity\TaxType $taxType */
        $taxTypeConfiguration = $taxType->getPluginConfiguration();
        $taxRates            += $taxTypeConfiguration['rates'];
      }

      if ($taxRates) {

        $form['yandex_checkout_tax_label'] = [
          '#type'  => 'html_tag',
          '#tag'   => 'label',
          '#value' => $this->t('Сопоставьте ставки'),
          '#state' => [
            'visible' => [
            [
                [':input[name="measurementmethod"]' => ['value' => '5']],
              'xor',
                [':input[name="measurementmethod"]' => ['value' => '6']],
              'xor',
                [':input[name="measurementmethod"]' => ['value' => '7']],
            ],
            ],
          ],

        ];

        $form['yandex_checkout_tax_wrapper_begin'] = [
          '#markup' => '<div>',
        ];

        $form['yandex_checkout_label_shop_tax'] = [
          '#markup' => t('<div style="float: left;width: 200px;">Ставка в вашем магазине.</div>'),
        ];

        $form['yandex_checkout_label_tax_rate'] = [
          '#markup' => t('<div>Ставка для чека в налоговую.</div>'),
        ];

        $form['yandex_checkout_tax_wrapper_end'] = [
          '#markup' => '</div>',
        ];

        foreach ($taxRates as $taxRate) {
          $form['yandex_checkout_tax']["yandex_checkout_tax_label_{$taxRate['id']}_begin"] = [
            '#markup' => '<div>',
          ];
          $form['yandex_checkout_tax']["yandex_checkout_tax_label_{$taxRate['id']}_lbl"]   = [
            '#prefix' => '<div style="width: 200px;float: left;padding-top: 5px;">',
            '#markup' => "<label>{$taxRate['label']}</label>",
            '#suffix' => '</div>',
          ];

          $defaultTaxValue = isset($this->configuration['yandex_checkout_tax'][$taxRate['id']])
                        ? $this->configuration['yandex_checkout_tax'][$taxRate['id']]
                        : 1;
          $form['yandex_checkout_tax'][$taxRate['id']] = [
            '#type'          => 'select',
            '#title'         => FALSE,
            '#label'         => FALSE,
            '#options'       => [
              1 => t('Без НДС'),
              2 => t('0%'),
              3 => t('10%'),
              4 => t('20%'),
              5 => t('Расчётная ставка 10/110'),
              6 => t('Расчётная ставка 20/120'),
            ],
            '#default_value' => $defaultTaxValue,
          ];

          $form['yandex_checkout_tax']["yandex_checkout_tax_label_{$taxRate['id']}_end"] = [
            '#markup' => '</div><br style="clear: both;">',
          ];
        }
      }
    }

  }

  /**
   * Processes the notification request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The response, or NULL to return an empty HTTP 200 response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \YooKassa\Common\Exceptions\ApiException
   * @throws \YooKassa\Common\Exceptions\BadApiRequestException
   * @throws \YooKassa\Common\Exceptions\ForbiddenException
   * @throws \YooKassa\Common\Exceptions\InternalServerError
   * @throws \YooKassa\Common\Exceptions\NotFoundException
   * @throws \YooKassa\Common\Exceptions\ResponseProcessingException
   * @throws \YooKassa\Common\Exceptions\TooManyRequestsException
   * @throws \YooKassa\Common\Exceptions\UnauthorizedException
   */
  public function onNotify(Request $request) {
    $rawBody = $request->getContent();
    $this->log('Notification: ' . $rawBody);
    $notificationData  = Json::decode($rawBody);
    $notificationModel = ($notificationData['event'] === NotificationEventType::PAYMENT_SUCCEEDED)
            ? new NotificationSucceeded($notificationData)
            : new NotificationWaitingForCapture($notificationData);
    $apiClient         = $this->apiClient;
    $paymentResponse   = $notificationModel->getObject();
    $paymentId         = $paymentResponse->id;
    $payment_storage   = \Drupal::entityTypeManager()->getStorage('commerce_payment');
    $payments          = $payment_storage->loadByProperties(['remote_id' => $paymentId]);
    if (!$payments) {
      return new Response('Bad request', 400);
    }
    /** @var \Drupal\commerce_payment\Entity\Payment $payment */
    $payment = reset($payments);
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = $payment->getOrder();
    if (!$order) {
      return new Response('Order not found', 404);
    }

    $paymentInfo = $apiClient->getPaymentInfo($paymentId);
    $this->log('Payment info: ' . Json::encode($paymentInfo));

    $state = $order->getState()->value;
    if ($state !== 'completed') {
      switch ($paymentInfo->status) {
        case PaymentStatus::WAITING_FOR_CAPTURE:
          $captureRequest  = CreateCaptureRequest::builder()->setAmount($paymentInfo->getAmount())->build();
          $captureResponse = $apiClient->capturePayment($captureRequest, $paymentId);
          $this->log('Payment info after capture: ' . Json::encode($captureResponse));
          if ($captureResponse->status == PaymentStatus::SUCCEEDED) {
            $payment->setRemoteState($paymentInfo->status);
            $order->state = 'completed';
            $order->setCompletedTime(\Drupal::time()->getRequestTime());
            $order->save();
            $payment->save();
            $this->log('Payment completed');

            return new Response('Payment completed', 200);
          }
          elseif ($captureResponse->status == PaymentStatus::CANCELED) {
            $payment->setRemoteState($paymentInfo->status);
            $payment->save();
            $this->log('Payment canceled');

            return new Response('Payment canceled', 200);
          }
          break;

        case PaymentStatus::PENDING:
          $payment->setRemoteState($paymentInfo->status);
          $payment->save();
          $this->log('Payment pending');

          return new Response(' Payment Required', 402);

        case PaymentStatus::SUCCEEDED:
          $payment->setRemoteState($paymentInfo->status);
          $order->state = 'completed';
          $order->setCompletedTime(\Drupal::time()->getRequestTime());
          $order->save();
          $payment->save();
          $this->log('Payment complete');

          return new Response('Payment complete', 200);

        case PaymentStatus::CANCELED:
          $payment->setRemoteState($paymentInfo->status);
          $payment->save();
          $this->log('Payment canceled');

          return new Response('Payment canceled', 200);
      }
    }

    return new Response('OK', 200);
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   Order entity.
   *
   * @return \Drupal\Core\Url
   *   The "cancel" page URL.
   */
  protected function buildCancelUrl(OrderInterface $order) {
    return Url::fromRoute('commerce_payment.checkout.cancel', [
      'commerce_order' => $order->id(),
      'step'           => 'payment',
    ], ['absolute' => TRUE]);
  }

  /**
   * Log.
   *
   * @param string $message
   *   Log message.
   */
  private function log($message) {
    \Drupal::logger('yandex_checkout')->info($message);
  }

}
